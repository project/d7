<?php
/**
 * @file
 * Drupal 7 backport, functions from common.inc
 */

/**
 * Get the name of the currently active install profile.
 *
 * When this function is called during Drupal's initial installation process,
 * the name of the profile that's about to be installed is stored in the global
 * installation state. At all other times, the standard Drupal systems variable
 * table contains the name of the current profile, and we can call variable_get()
 * to determine what one is active.
 *
 * @return $profile
 *   The name of the install profile.
 */
function drupal_get_profile() {
  global $install_state;

  if (isset($install_state['parameters']['profile'])) {
    $profile = $install_state['parameters']['profile'];
  }
  else {
    $profile = variable_get('install_profile', 'standard');
  }

  return $profile;
}


/**
 * Get the entity info array of an entity type.
 *
 * @see hook_entity_info()
 * @see hook_entity_info_alter()
 *
 * @param $entity_type
 *   The entity type, e.g. node, for which the info shall be returned, or NULL
 *   to return an array with info about all types.
 */
function entity_get_info($entity_type = NULL) {
  // Use the advanced drupal_static() pattern, since this is called very often.
  static $drupal_static_fast;
  if (!isset($drupal_static_fast)) {
    $drupal_static_fast['entity_info'] = &drupal_static(__FUNCTION__);
  }
  $entity_info = &$drupal_static_fast['entity_info'];

  if (empty($entity_info)) {
    if ($cache = cache_get('entity_info')) {
      $entity_info = $cache->data;
    }
    else {
      $entity_info = module_invoke_all('entity_info');
      // Merge in default values.
      foreach ($entity_info as $name => $data) {
        $entity_info[$name] += array(
          'fieldable' => FALSE,
          'controller class' => 'DrupalDefaultEntityController',
          'static cache' => TRUE,
          'load hook' => $name . '_load',
          'bundles' => array(),
          'view modes' => array(),
          'object keys' => array(),
          'cacheable' => TRUE,
          'translation' => array(),
        );
        $entity_info[$name]['object keys'] += array(
          'revision' => '',
          'bundle' => '',
        );
        // If no bundle key is provided, assume a single bundle, named after
        // the entity type.
        if (empty($entity_info[$name]['object keys']['bundle']) && empty($entity_info[$name]['bundles'])) {
          $entity_info[$name]['bundles'] = array($name => array('label' => $entity_info[$name]['label']));
        }
      }
      // Let other modules alter the entity info.
      drupal_alter('entity_info', $entity_info);
      cache_set('entity_info', $entity_info);
    }
  }

  if (empty($entity_type)) {
    return $entity_info;
  }
  elseif (isset($entity_info[$entity_type])) {
    return $entity_info[$entity_type];
  }
}

/**
 * Resets the cached information about entity types.
 */
function entity_info_cache_clear() {
  drupal_static_reset('entity_get_info');
  cache_clear_all('entity_info', 'cache');
}

/**
 * Helper function to extract id, vid, and bundle name from an entity.
 *
 * @param $entity_type
 *   The entity type; e.g. 'node' or 'user'.
 * @param $entity
 *   The entity from which to extract values.
 * @return
 *   A numerically indexed array (not a hash table) containing these
 *   elements:
 *   0: primary id of the entity
 *   1: revision id of the entity, or NULL if $entity_type is not versioned
 *   2: bundle name of the entity
 *   3: whether $entity_type's fields should be cached (TRUE/FALSE)
 */
function entity_extract_ids($entity_type, $entity) {
  $info = entity_get_info($entity_type);
  // Objects being created might not have id/vid yet.
  $id = isset($entity->{$info['object keys']['id']}) ? $entity->{$info['object keys']['id']} : NULL;
  $vid = ($info['object keys']['revision'] && isset($entity->{$info['object keys']['revision']})) ? $entity->{$info['object keys']['revision']} : NULL;
  // If no bundle key provided, then we assume a single bundle, named after the
  // entity type.
  $bundle = $info['object keys']['bundle'] ? $entity->{$info['object keys']['bundle']} : $entity_type;
  $cacheable = $info['cacheable'];
  return array($id, $vid, $bundle, $cacheable);
}

/**
 * Helper function to assemble an object structure with initial ids.
 *
 * This function can be seen as reciprocal to entity_extract_ids().
 *
 * @param $entity_type
 *   The entity type; e.g. 'node' or 'user'.
 * @param $ids
 *   A numerically indexed array, as returned by entity_extract_ids(),
 *   containing these elements:
 *   0: primary id of the entity
 *   1: revision id of the entity, or NULL if $entity_type is not versioned
 *   2: bundle name of the entity
 * @return
 *   An entity structure, initialized with the ids provided.
 */
function entity_create_stub_entity($entity_type, $ids) {
  $entity = new stdClass();
  $info = entity_get_info($entity_type);
  $entity->{$info['object keys']['id']} = $ids[0];
  if (isset($info['object keys']['revision']) && !is_null($ids[1])) {
    $entity->{$info['object keys']['revision']} = $ids[1];
  }
  if ($info['object keys']['bundle']) {
    $entity->{$info['object keys']['bundle']} = $ids[2];
  }
  return $entity;
}

/**
 * Load entities from the database.
 *
 * This function should be used whenever you need to load more than one entity
 * from the database. The entities are loaded into memory and will not require
 * database access if loaded again during the same page request.
 *
 * The actual loading is done through a class that has to implement the
 * DrupalEntityController interface. By default, DrupalDefaultEntityController
 * is used. Entity types can specify that a different class should be used by
 * setting the 'controller class' key in hook_entity_info(). These classes can
 * either implement the DrupalEntityController interface, or, most commonly,
 * extend the DrupalDefaultEntityController class. See node_entity_info() and
 * the NodeController in node.module as an example.
 *
 * @see hook_entity_info()
 * @see DrupalEntityController
 * @see DrupalDefaultEntityController
 *
 * @param $entity_type
 *   The entity type to load, e.g. node or user.
 * @param $ids
 *   An array of entity IDs, or FALSE to load all entities.
 * @param $conditions
 *   An array of conditions in the form 'field' => $value.
 * @param $reset
 *   Whether to reset the internal cache for the requested entity type.
 *
 * @return
 *   An array of entity objects indexed by their ids.
 */
function entity_load($entity_type, $ids = array(), $conditions = array(), $reset = FALSE) {
  if ($reset) {
    entity_get_controller($entity_type)->resetCache();
  }
  return entity_get_controller($entity_type)->load($ids, $conditions);
}

/**
 * Get the entity controller class for an entity type.
 */
function entity_get_controller($entity_type) {
  $controllers = &drupal_static(__FUNCTION__, array());
  if (!isset($controllers[$entity_type])) {
    $type_info = entity_get_info($entity_type);
    $class = $type_info['controller class'];
    $controllers[$entity_type] = new $class($entity_type);
  }
  return $controllers[$entity_type];
}

/**
 * Invoke hook_entity_prepare_view().
 *
 * If adding a new entity similar to nodes, comments or users, you should
 * invoke this function during the ENTITY_build_content() or
 * ENTITY_view_multiple() phases of rendering to allow other modules to alter
 * the objects during this phase. This is needed for situations where
 * information needs to be loaded outside of ENTITY_load() - particularly
 * when loading entities into one another - i.e. a user object into a node, due
 * to the potential for unwanted side-effects such as caching and infinite
 * recursion. By convention, entity_prepare_view() is called after
 * field_attach_prepare_view() to allow entity level hooks to act on content
 * loaded by field API.
 * @see hook_entity_prepare_view()
 *
 * @param $entity_type
 *   The type of entity, i.e. 'node', 'user'.
 * @param $entities
 *   The entity objects which are being prepared for view, keyed by object ID.
 */
function entity_prepare_view($entity_type, $entities) {
  // To ensure hooks are only run once per entity, check for an
  // entity_view_prepared flag and only process items without it.
  // @todo: resolve this more generally for both entity and field level hooks.
  $prepare = array();
  foreach ($entities as $id => $entity) {
    if (empty($entity->entity_view_prepared)) {
      // Add this entity to the items to be prepared.
      $prepare[$id] = $entity;

      // Mark this item as prepared.
      $entity->entity_view_prepared = TRUE;
    }
  }

  if (!empty($prepare)) {
    module_invoke_all('entity_prepare_view', $prepare, $entity_type);
  }
}

/**
 * Returns the uri elements of an entity.
 *
 * @param $entity_type
 *   The entity type; e.g. 'node' or 'user'.
 * @param $entity
 *   The entity for which to generate a path.
 * @return
 *   An array containing the 'path' and 'options' keys used to build the uri of
 *   the entity, and matching the signature of url(). NULL if the entity has no
 *   uri of its own.
 */
function entity_uri($entity_type, $entity) {
  $info = entity_get_info($entity_type);
  if (isset($info['uri callback']) && function_exists($info['uri callback'])) {
    return $info['uri callback']($entity) + array('options' => array());
  }
}
/**
 * Invokes entity insert/update hooks.
 *
 * @param $op
 *   One of 'insert' or 'update'.
 * @param $entity_type
 *   The entity type; e.g. 'node' or 'user'.
 * @param $entity
 *   The entity object being operated on.
 */
function entity_invoke($op, $entity_type, $entity) {
  module_invoke_all('entity_' . $op, $entity, $entity_type);
}
