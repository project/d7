<?php
/**
 * @file
 * D7 API backport
 */

/**
 * Helper function to scan and collect module .info data.
 *
 * @return
 *   An associative array of module information.
 */
function _system_rebuild_module_data() {
  // Find modules
  $modules = drupal_system_listing('/\.module$/', 'modules', 'name', 0);

  // Include the install profile in modules that are loaded.
  $profile = drupal_get_profile();
  $modules[$profile] = new stdClass;
  $modules[$profile]->name = $profile;
  $modules[$profile]->uri = 'profiles/' . $profile . '/' . $profile . '.profile';
  $modules[$profile]->filename = $profile . '.profile';

  // Install profile hooks are always executed last.
  $modules[$profile]->weight = 1000;

  // Set defaults for module info.
  $defaults = array(
    'dependencies' => array(),
    'dependents' => array(),
    'description' => '',
    'package' => 'Other',
    'version' => NULL,
    'php' => DRUPAL_MINIMUM_PHP,
    'files' => array(),
    'bootstrap' => 0,
  );

  // Read info files for each module.
  foreach ($modules as $key => $module) {
    // The module system uses the key 'filename' instead of 'uri' so copy the
    // value so it will be used by the modules system.
    $modules[$key]->filename = $module->uri;

    // Look for the info file.
    $module->info = drupal_parse_info_file(dirname($module->uri) . '/' . $module->name . '.info');

    // Skip modules that don't provide info.
    if (empty($module->info)) {
      unset($modules[$key]);
      continue;
    }

    // Merge in defaults and save.
    $modules[$key]->info = $module->info + $defaults;

    // Invoke hook_system_info_alter() to give installed modules a chance to
    // modify the data in the .info files if necessary.
    $type = 'module';
    drupal_alter('system_info', $modules[$key]->info, $modules[$key], $type);
  }

  // The install profile is required.
  $modules[$profile]->info['required'] = TRUE;

  return $modules;
}

/**
 * Rebuild, save, and return data about all currently available modules.
 *
 * @return
 *   Array of all available modules and their data.
 */
function system_rebuild_module_data() {
  $modules = _system_rebuild_module_data();
  ksort($modules);
  system_get_files_database($modules, 'module');
  //system_update_files_database($modules, 'module');
  $modules = _module_build_dependencies($modules);
  return $modules;
}

